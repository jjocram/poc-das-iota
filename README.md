# Proof of Concept of the Decentralized Authorization Service in IOTA v2
This repository contains different submodules, one for each component. To download it you need to run the following command:
```bash
git clone --recurse-submodules git@gitlab.com:jjocram/poc-das-iota.git
```

## Create .env file
To store some variables a .env file is used.
```bash
touch .env
```
Fill it with:
```bash
export USER_PRIVATE_KEY=""
export USER_ADDRESS=""
export SSPERMISSION_ADDRESS=""
export ADDRESS_SHARING_ADDRESS=""
```

## Start WASP node and Goshimmer Network
1. `cd wasp/tools/devnet`
2. `docker-compose up`

## Install Metamask
Install Metamask on your browser [https://metamask.io](https://metamask.io)

Set `USER_PRIVATE_KEY` and `USER_ADDRESS` in `.env` with the private key and the address of the Metamask user created. Source it
```bash
source .env
```

## Create the EVM chain
Remember to source the `.env` file
1. `cd wasp/tools/wasp-cli`
2. Create a file named `wasp-cli.json` with this content:
```json
{
  "goshimmer": {
    "api": "127.0.0.1:8080",
    "faucetpowtarget": -1
  },
  "wasp": {
    "0": {
      "api": "127.0.0.1:9090",
      "nanomsg": "127.0.0.1:5550",
      "peering": "127.0.0.1:4000"
    }
  }
}

```
3. `go run main.go init`
4. `go run main.go peering info` remember PubKey and NetID
5. `go run main.go peering trust <PubKey> <NetID>` PubKey and NetID are from the previous output
6. `go run main.go request-funds`
7. `go run main.go chain deploy --committee=0 --quorum=1 --chain=evmchain --description="EVM Chain"`
8. `go run main.go chain deposit IOTA:10000`
9. `go run main.go chain evm deploy -a evmchain --alloc $USER_ADDRESS:1000000000000000000000000`
10. `go run main.go chain evm jsonrpc --chainid 1074`

## Deploy the smart contracts
- Remember to source the `.env` file
- Install dependecies
  - `npm install`


1. `cd das-smart-contracts/`
2. `npx hardhat run scripts/deploy.js --network local`
3. Copy the addresses of the deployed smart contracts in `.env`
4. Source the `.env` file

## Start the "dumb" Authorization Service
- Remember to source the `.env` file
- Install dependecies (use a virtualenv for semplicity)
  - `pip install -r requirements.txt`


1. `cd das-dumb-authorization-service/`
2. `python app.py`

## Start the website
- Remember to source the `.env` file
- Install dependecies
  - `npm install`


1. `cd das-frontend/`
2. `npm start`
